#!bin bash
# The script must be run from scripts folder located in transcriptomic-project folder
#date: 14 april 2022
#Autor: Nooshin Bayat
#Transcriptomic project Apartado1
cd ..
cd Apartado1
cd input
export WD=$(pwd)

#quality check:
mkdir -p fastQC
fastqc -o fastQC --noextract *.fastq

#reference indexing:
hisat2-build --seed 123 -p 2 REF/chr21.fa REF/chr21_GRCh38

#sample mapping:
mkdir -p hisat2
for sid in $(ls *.fastq | cut -d "." -f1); do hisat2 --new-summary --summary-file hisat2/$sid.hisat2.summary  --rna-strandness R --seed 123 --phred33 -p 2 -k 1 -x REF/chr21_GRCh38 -U $sid.chr21.fastq -S hisat2/$sid.sam; done

#mapped reads counts:
cd hisat2
for sid in $(ls *.sam | cut -d "." -f1);do samtools view -bh -S $sid.sam > $sid.bam; done
for sid in $(ls *.sam | cut -d "." -f1); do samtools sort $sid.bam -o $sid.sorted.bam; done
for sid in $(ls *.sam | cut -d "." -f1);do samtools index $sid.sorted.bam; done

#HTseq-count to count reads
#go back to input directory
cd ..
mkdir -p htseq
for sid in $(ls *.fastq | cut -d "." -f1); do htseq-count --format=bam --stranded=reverse --mode=intersection-nonempty --minaqual=10 --type=exon --idattr=gene_id --additional-attr=gene_name hisat2/$sid.sorted.bam REF/GRCh38.gencode.v38.annotation.for.chr21.gtf > htseq/$sid.htseq; done
#execute Rstudio in input folder:
#go back to the general directory in transcriptomics-project/
cd ..
cd ..
cd scripts
Rscript apartado_1_3.R
